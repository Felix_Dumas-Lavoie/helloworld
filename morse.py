import time
import RPi.GPIO as GPIO
import sys

#Un tiret est égal à trois points
#L'espacement entre deux éléments d'une
#même lettre est égal à trois points

#Déclaration d'un dictionnaire des symboles
d = {}
d['a'] = '.-'
d['b'] = '-...'
d['c'] = '-.-.'
d['d'] = '-..'
d['e'] = '.'
d['f'] = '..-.'
d['g'] = '--.'
d['h'] = '....'
d['i'] = '..'
d['j'] = '.---'
d['k'] = '-.-'
d['l'] = '.-..'
d['m'] = '--'
d['n'] = '-.'
d['o'] = '---'
d['p'] = '.--.'
d['q'] = '--.-'
d['r'] = '.-.'
d['s'] = '...'
d['t'] = '-'
d['u'] = '..-'
d['v'] = '...-'
d['w'] = '.--'
d['x'] = '-..-'
d['y'] = '-.--'
d['z'] = '--..'
d[' '] = ' '
d['1'] = '.----'
d['2'] = '..---'
d['3'] = '...--'
d['4'] = '....-'
d['5'] = '.....'
d['6'] = '-....'
d['7'] = '--...'
d['8'] = '---..'
d['9'] = '----.'
d['0'] = '-----'

#Déclaration des durées (en ms) 
noPine = int(sys.argv[1])
POINT = 0.2
TIRET = 3*POINT
ETEINTDANSLETTRE = POINT
ESPACELETTRE = 3*POINT
ESPACEMOTS = 7*POINT-ESPACELETTRE

def initialisationGPIO(noPine):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(noPine,GPIO.OUT)

def lightOn(noPine):
	GPIO.output(noPine,GPIO.HIGH)

def lightOff(noPine):
	GPIO.output(noPine,GPIO.LOW)
	
def codeMorse():
	code = input("Veuillez entrer un mot ou une phrase S.V.P. ")
	print("Ce mot ou cette phrase sera maintenant convertie en code MORSE")
	liste = []
	i = 0
	while (i< len(code)):
		uneLettre = ''
		uneLettre = d[code[i]]
		liste.append(uneLettre)
		i += 1
	return liste 
	

def lectureLettre(codeLettre):
        for i in range(len(codeLettre)):
                if (codeLettre[i] == '-'):
                        lightOn(noPine)
                        time.sleep(TIRET)
                elif (codeLettre[i] == '.'):
                        lightOn(noPine)
                        time.sleep(POINT)
                if (len(codeLettre)-i == 1):
                        lightOff(noPine)
                        time.sleep(ESPACELETTRE)
                else:
                        lightOff(noPine)
                        time.sleep(ETEINTDANSLETTRE)



def main():                
                try:
                        liste = codeMorse()
                        print(liste)

                        initialisationGPIO(noPine)

                        for i in range(len(liste)):
                                if (liste[i] == ' '):
                                        time.sleep(ESPACEMOTS)
                                else:
                                        lectureLettre(liste[i])
                                
                except:
                        GPIO.output(noPine,GPIO.LOW)
                        
main()

	
